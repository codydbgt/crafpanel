<?php 

function UrlReturn($url) {
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}

function isValid($token){
   $return = UrlReturn($sessionHome."?token=$token");
   
	if($return == "TRUE"){
	 return true;
	}
	return false;
}

function login($user,$pass){
	global $blowfish;
	if(preg_match("/^[a-z0-9_-]{3,16}$/",$user)){
		if(preg_match("/^[a-z0-9_-]{3,18}$/",$pass)){
		//valid name and pass :) now to hash :) 
		$hash = hash("sha256",$user.$pass.$blowfish);
		$_SESSION['name']=$user;
		$_SESSION['hash']=$hash;
		}else{
			showerror("Invalid password");
		}
	}else{
		showerror("Invalid Username");
	}
}

function register($user,$pass){
	global $sql,$blowfish;
	if(preg_match("/^[a-z0-9_-]{3,16}$/",$user)){
		if(preg_match("/^[a-z0-9_-]{4,18}$/",$pass)){
			//valid name and pass :) now to hash :) 
			$hash = hash("sha256",$user.$pass.$blowfish);
			clog($hash);
			$u = new user($user);
			
			if($u->isvalid()){
				showerror("User Already is registered!");
				return false;
			}

			if($sql->query("INSERT INTO User(Name,Hash) VALUES('$user','$hash')") === TRUE) {
				return true;
			} else {
				showerror("Error: ".$sql->error."<br>");
				return false;
			}
				return true;
		}else{
			showerror("Invalid Password");
		}
	}else{
		showerror("Invalid Username");
	}

return false;	
}


function logout(){
	global $page;
	if(!($page==="login" || $page==="register" ) ){	
	echo "<script>setTimeout(\"location.href = 'index.php?p=login';\",10);</script>";
	}
}

function notifyLeft($title,$text){
echo '<script> notifyLeft("'.$title.'","'.$text.'");</script>';
}

function success($msg){
	$_SESSION['success'] = $msg;
}

function info($msg){
	$_SESSION['info']= $msg;
}

function showerror($msg){
$_SESSION['error'] = $msg;	
}

function clog($msg){
echo '<script>console.log("'.$msg.'");</script>';
}

?>