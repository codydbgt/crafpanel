<?php
ob_start();
#Global vars
$page = "home";
$sql;
$user;
?>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>CraftPanel</title>
	<meta name="description" content="CraftPanel Dashboard">
	<meta name="author" content="codydbgt">
	<meta name="keyword" content="CraftPanel">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
	
</head>

<body>
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
		
		<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
		<script src="js/jquery.flot.js"></script>
		<script src="js/jquery.flot.pie.js"></script>
		<script src="js/jquery.flot.stack.js"></script>
		<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
		
		<script src="js/socket.io-1.3.5.js"></script>
	<!-- end: JavaScript-->
</body>
<?php
include_once("config.php");
include_once("Functions/DBmanager.php");
include_once("Functions/DefFunctions.php");
include_once("Classes/c_user.php");

db_connect();
if (isset($_GET["p"]))
$page=$_GET["p"];

session_start();

//process register 
if(isset($_POST['r_username']) && isset($_POST['r_password']) ){
	//validate passwrod and username 
	if(register($_POST['r_username'],$_POST['r_password']) ){
	success("You are now Registered!");	
	}
}

//-- process login at session start --//
if(isset($_POST['username']) && isset($_POST['password']) ){
	login($_POST['username'],$_POST['password']);
//-- if the login is vaild then a session user name will be set along with stored hash has the following format hash(password+username+blowfish)
}

if(!isset($_SESSION['ip'])) 
$_SESSION['ip']=$_SERVER['REMOTE_ADDR'];

if(!$_SESSION['ip']==$_SERVER['REMOTE_ADDR']){
$_SESSION['error']="Bad Session ip";
logout();
}

if($page!="register"){
	if(!isset($_SESSION['name'])){
		//$_SESSION['error']="You are not logged in";
		logout();
	}
}
//-- if name set we validate the session --//
if(isset($_SESSION['name'])){
	global $user;
	$user = new user($_SESSION['name']);
	if(!$user->isvalid()){
	$_SESSION['name']=null;
	$_SESSION['error']="Bad Session Please relog";
	logout();
	}	
	//$GLOBALS['user']=$user;
	
	if($user->Isvalid() & $user->Logedin() ){
	//-- Below is a valid session call any user API call should work at this point --//	
	?>
	<script>
	var token =  <?php echo '"'.$_SESSION['token'].'"'; ?>;
	var socket =  io.connect('http://localhost:9092');

	socket.on('connect', function() { 
        console.log('connected to server..requesting login'); 
        socket.emit('login',{message:token}); 
    });
	
	socket.on('debug', function(data) { 
		console.log(data.message); 
    });
	
	</script>
	<?php
	
	//-- valid session above --//	
	}
}
//popup stuff 
if(isset($_GET['p'])){
	if(isset($_SESSION['error'])){
	echo '<script> ShowError("'.$_SESSION['error'].'");</script>';
	$_SESSION['error']=null;
	}

	if(isset($_SESSION['success'])){
	echo '<script> success("'.$_SESSION['success'].'");</script>';
	$_SESSION['success']=null;
	}


	if(isset($_SESSION['info'])){
	echo '<script> Showinfo("INFO: '.$_SESSION['info'].'");</script>';
	$_SESSION['info']=null;
	}

	if(file_exists("pages/$page.php")){
	include("pages/$page.php");
	}else{
	header( 'Location: index.php?p=error&error=1');
	}
}else{
	echo "<script>setTimeout(\"location.href = 'index.php?p=home';\",100);</script>";
}

$sql->close();
?>