<?php
//SEND BACK IF LOGEDIN
if(isset($_SESSION['user']))
gotopage('login');
?>

<style type="text/css">
body { background: url(img/bg-login.jpg) !important; }
</style>

<div class="container-fluid-full">
	<div class="row-fluid">
				
		<div class="row-fluid">
			<div class="login-box">
				<div class="icons">
					<a href="index.php"><i class="halflings-icon home"></i></a>
				</div>
				<h2>Make a new account</h2>
				<form class="form-horizontal" action="index.php" method="post">
					<fieldset>
						
						<div class="input-prepend">
							<span class="add-on"><i class="halflings-icon user">username</i></span>
							<input class="input-large span10" name="r_username" id="username" type="text" placeholder="username"/>
						</div>
						<div class="clearfix"></div>

						<div class="input-prepend">
							<span class="add-on"><i class="halflings-icon lock">password</i></span>
							<input class="input-large span10 checkpass" name="r_password" id="password" type="password" placeholder="password"/>
							<h2 id="checkpassinfo"></h2>
						</div>
						<div class="clearfix"></div>
						
						<div class="input-prepend">
							<span class="add-on"><i class="halflings-icon lock">password</i></span>
							<input class="input-large span10 checkpass" name="repeat" id="repeat" type="password" placeholder="repeat password"/>
						</div>
						<div class="clearfix"></div>

						<div class="button-login">	
							<button type="submit" id="register" class="btn btn-primary">Register</button>
						</div>
						<div class="clearfix"></div>
				</form>
			</div><!--/span-->
		</div><!--/row-->
			

	</div><!--/.fluid-container-->
	
</div><!--/fluid-row-->
